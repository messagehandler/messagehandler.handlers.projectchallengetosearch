namespace ProjectChallengesToSearch
{
    public enum ChallengePublicationState
    {
        Draft,
        Published
    }
}