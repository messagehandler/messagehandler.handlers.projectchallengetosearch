using MessageHandler;
using MessageHandler.SDK.EventSource;
using MessageHandler.Contracts.Challenges;

namespace ProjectChallengesToSearch
{
    public class Projection :
        IProjection<SearchEntry, ChallengeDrafted>,
        IProjection<SearchEntry, ChallengePublished>,
        IProjection<SearchEntry, ChallengeUnpublished>,
        IProjection<SearchEntry, ChallengeUpdated>
    {
        public void Project(SearchEntry record, ChallengeDrafted msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Id = msg.Details.ChallengeId;
            record.User = msg.Details.AuthorId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Tenant = msg.Details.TenantId;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Challenge";
        }

        public void Project(SearchEntry record, ChallengePublished msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Id = msg.Details.ChallengeId;
            record.User = msg.Details.AuthorId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Tenant = msg.Details.TenantId;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Challenge";
        }

        public void Project(SearchEntry record, ChallengeUnpublished msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Id = msg.Details.ChallengeId;
            record.User = msg.Details.AuthorId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Tenant = msg.Details.TenantId;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Challenge";
        }

        public void Project(SearchEntry record, ChallengeUpdated msg)
        {
            record.Filter1 = msg.Details.Author;
            record.Id = msg.Details.ChallengeId;
            record.User = msg.Details.AuthorId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Tenant = msg.Details.TenantId;
            record.Object = Json.Encode(msg.Details);
            record.Type = "Challenge";
        }
    }
}