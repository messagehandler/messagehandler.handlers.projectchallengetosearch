using MessageHandler;
using MessageHandler.SDK.EventSource;

namespace ProjectChallengesToSearch
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectChallengesToSearchConfig>();
            container.UseEventSourcing(config.ConnectionString)
                    .EnableProjections(config.ConnectionString);
        }
    }
}